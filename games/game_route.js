const express = require("express");
const gameRouter = express.Router();
const gameController = require("./game_controller");
const authMiddleware = require("../middleware/auth_middleware");

gameRouter.get("/game", authMiddleware, gameController.getAllGames);

gameRouter.post("/room", gameController.createARoom);

module.exports = gameRouter;
