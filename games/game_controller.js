const gameModel = require("./game_model");

class GameController {
  getAllGames = (req, res) => {
    const allGame = gameModel.getAllGame();
    return res.json(allGame);
  };

  createARoom = (req, res) => {
    const dataRegistered = req.body;
    if (
      dataRegistered.roomName === undefined ||
      dataRegistered.roomName === ""
    ) {
      res.statusCode = 400;
      return res.json({ message: "Room name must be filled" });
    }
    if (dataRegistered.roomName.length > 100) {
      res.statusCode = 400;
      return res.json({ message: "Room name must not exceed 100 characters" });
    }

    // const totalRoom = gameModel.stopCreateRoom({ id });
    // if (totalRoom > 10) {
    //   res.statusCode = 400;
    //   return res.json({ message: "All room occupied" });
    // }

    const existRoom = gameModel.isRoomExisted(dataRegistered);
    if (existRoom) {
      res.statusCode = 400;
      return res.json({ message: "Room is already exist" });
    }

    gameModel.creatingRoom(dataRegistered);

    return res.json({ message: "Room is made" });
  };
}

module.exports = new GameController();
