const { body } = require("express-validator");

const registrationSchema = [
  body("username")
    .notEmpty()
    .withMessage("Username must be filled")
    .isString()
    .withMessage("Must be character")
    .isLength({ max: 20 })
    .withMessage("Username must not exceed 20 characters")
    .matches(/^[a-zA-Z0-9@._]+$/)
    .withMessage("Username can only contain letters, numbers, @, ., and _"),
  body("email")
    .notEmpty()
    .withMessage("Email must be filled")
    .isEmail()
    .withMessage("Invalid email format"),
  body("password")
    .notEmpty()
    .withMessage("Password must be filled")
    .isLength({ min: 8 })
    .isStrongPassword({
      options: {
        minLength: 8,
        minUppercase: 1,
        minNumbers: 1,
        minSymbols: 1,
      },
    })
    .withMessage(
      "Password must have at least 8 characters and contain uppercase, number, and symbol"
    ),
];

const loginSchema = [
  body("username").notEmpty().withMessage("Username must be filled"),
  body("password").notEmpty().withMessage("Password must be filled"),
];

const updateBioSchema = [
  body("fullname").notEmpty().withMessage("Fullname must be filled"),
  body("phoneNumber")
    .notEmpty()
    .withMessage("Phone number must be filled")
    .isInt()
    .withMessage("Numbers only"),
  body("address").notEmpty().withMessage("Address must be filled"),
  body("dateOfBirth").notEmpty().withMessage("Date of birth must be filled"),
];

const gameHisSchema = [
  body("userID")
    .notEmpty()
    .withMessage("User ID must be filled")
    .isInt()
    .withMessage("Numbers only"),
  body("status").notEmpty().withMessage("Status must be filled"),
];

module.exports = {
  registrationSchema,
  loginSchema,
  updateBioSchema,
  gameHisSchema,
};
