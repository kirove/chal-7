const userModel = require("./user_model");
const md5 = require("md5");
const jwt = require("jsonwebtoken");

class UserController {
  getAllUsers = async (req, res) => {
    const allUser = await userModel.getAllUser();
    return res.json(allUser);
  };

  getSingleUser = async (req, res) => {
    const { id } = req.params;
    try {
      const singleUser = await userModel.getSingleUser(id);
      if (singleUser === null) {
        res.statusCode = 400;
        return res.json({ message: "User cannot be found" });
      } else {
        return res.json(singleUser);
      }
    } catch (error) {
      res.statusCode = 500;
      return res.send("Something went wrong");
    }
  };

  registerUser = async (req, res) => {
    const dataRegistered = req.body;
    try {
      const existedUser = await userModel.isUserRegistered(dataRegistered);

      if (existedUser) {
        res.statusCode = 400;
        return res.json({ message: "Username or email is exist" });
      }
      userModel.addingUser(dataRegistered);
      return res.json({ message: "New user has been added" });
    } catch (error) {
      res.statusCode = 500;
      return res.send("Something went wrong");
    }
  };

  loginForUser = async (req, res) => {
    const dataLogin = req.body;
    const userLogin = await userModel.verifyLogin(dataLogin);
    try {
      if (userLogin) {
        const token = jwt.sign({ ...userLogin, role: "player" }, "sssss", {
          expiresIn: "1d",
        });
        return res.json({ accessToken: token });
        // return res.json(userLogin);
      } else {
        res.statusCode = 400;
        return res.json({ message: "Username or Password is incorrect" });
      }
    } catch (error) {
      console.log(userLogin);
      res.statusCode = 500;
      return res.send("Something went wrong");
    }
  };

  updateUserBio = async (req, res) => {
    const { id } = req.params;
    const { fullname, phoneNumber, address, dateOfBirth } = req.body;
    try {
      const singleUser = await userModel.getSingleUser(id);
      if (singleUser === null) {
        res.statusCode = 400;
        return res.json({ message: "User cannot be found" });
      }
      const existUser = await userModel.isUserBioExist(id);
      if (existUser) {
        userModel.upsertUserBio(id, fullname, address, phoneNumber);
      } else {
        const data = await userModel.createUserBio(
          id,
          fullname,
          phoneNumber,
          address,
          dateOfBirth
        );
      }
      return res.json({ message: "User profile has been updated" });
    } catch (error) {
      res.statusCode = 500;
      return res.send("Something went wrong");
    }
  };

  createGameHistory = async (req, res) => {
    const dataGameHistory = req.body;
    try {
      if (
        dataGameHistory.userID === undefined ||
        dataGameHistory.userID === ""
      ) {
        res.statusCode = 400;
        return res.json({ message: "UserID must be filled" });
      }
      if (
        dataGameHistory.status === undefined ||
        dataGameHistory.status === ""
      ) {
        res.statusCode = 400;
        return res.json({ message: "Status must be filled" });
      }
      const dataUserGameHistory = await userModel.addingUserGameHistory(
        dataGameHistory
      );
      return res.json({ message: "Game history has been added" });
    } catch (error) {
      res.statusCode = 500;
      return res.send("Something went wrong");
    }
  };

  getUserGameHistories = async (req, res) => {
    const { id } = req.params;
    try {
      const userGameHistory = await userModel.getUserGameHistory(id);
      if (userGameHistory === null || userGameHistory.length === 0) {
        res.statusCode = 400;
        return res.json({ message: "User game history cannot be found" });
      } else {
        return res.json(userGameHistory);
      }
    } catch (error) {
      res.statusCode = 500;
      return res.send("Something went wrong");
    }
  };
}

module.exports = new UserController();
