const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

class UserModel {
  getAllUser = async () => {
    const dataAllUser = await db.User.findAll({
      include: [db.UserBio, db.UserGameHistory],
    });
    return dataAllUser;
  };

  getSingleUser = async (id) => {
    return await db.User.findOne({
      where: { id: id },
      include: [db.UserBio],
    });
  };

  isUserRegistered = async (dataRegistered) => {
    const exisedtUser = await db.User.findOne({
      where: {
        [Op.or]: [
          { username: dataRegistered.username },
          { email: dataRegistered.email },
        ],
      },
    });

    if (exisedtUser) {
      return true;
    } else {
      return false;
    }
  };

  addingUser = (dataRegistered) => {
    db.User.create({
      username: dataRegistered.username,
      email: dataRegistered.email,
      password: md5(dataRegistered.password),
    });
  };

  verifyLogin = async (dataLogin) => {
    const dataUser = await db.User.findOne({
      where: {
        username: dataLogin.username,
        password: md5(dataLogin.password),
      },
      attributes: { exclude: ["password"] },
      raw: true,
    });
    return dataUser;
  };

  isUserBioExist = async (id) => {
    const existData = await db.UserBio.findOne({
      where: {
        id: id,
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  createUserBio = async (id, fullname, phoneNumber, address, dateOfBirth) => {
    return await db.UserBio.create({
      userID: id,
      fullname: fullname,
      phoneNumber: phoneNumber,
      address: address,
      dateOfBirth: dateOfBirth,
    });
  };

  upsertUserBio = async (id, fullname, phoneNumber, address, dateOfBirth) => {
    return await db.UserBio.upsert({
      userID: id,
      fullname: fullname,
      phoneNumber: phoneNumber,
      address: address,
      dateOfBirth: dateOfBirth,
    });
  };

  addingUserGameHistory = (dataGameHistory) => {
    db.UserGameHistory.create({
      userID: dataGameHistory.userID,
      status: dataGameHistory.status,
    });
  };

  getUserGameHistory = async (id) => {
    return await db.User.findAll({
      include: [db.UserBio, db.UserGameHistory],
      where: { id: id },
    });
  };
}

module.exports = new UserModel();
