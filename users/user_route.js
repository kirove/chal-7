const express = require("express");
const userRouter = express.Router();
const userController = require("./user_controller");
const authMiddleware = require("../middleware/auth_middleware");
const protectMiddleware = require("../middleware/protect_middleware");
const schemaValidation = require("../middleware/schema_validation");
const userSchema = require("./user_schema");

userRouter.get("/user", authMiddleware, userController.getAllUsers);

userRouter.get(
  "/detail/:id",
  authMiddleware,
  protectMiddleware,
  userController.getSingleUser
);

userRouter.post(
  "/registration",
  userSchema.registrationSchema,
  schemaValidation,
  userController.registerUser
);

userRouter.post(
  "/login",
  userSchema.loginSchema,
  schemaValidation,
  userController.loginForUser
);

userRouter.put(
  "/detail/:id",
  userSchema.updateBioSchema,
  schemaValidation,
  authMiddleware,
  protectMiddleware,
  userController.updateUserBio
);

userRouter.post(
  "/gamehis",
  userSchema.gameHisSchema,
  schemaValidation,
  authMiddleware,
  userController.createGameHistory
);

userRouter.get(
  "/gamehis/:id",
  authMiddleware,
  protectMiddleware,
  userController.getUserGameHistories
);

module.exports = userRouter;
