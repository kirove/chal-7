const protectMiddleware = (req, res, next) => {
  const { id } = req.params;
  const requestedId = req.user.id.toString();

  if (requestedId === id) {
    next();
  } else {
    return res.send({ message: "Not Authorized" });
  }
};

module.exports = protectMiddleware;
