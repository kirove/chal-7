const jwt = require("jsonwebtoken");

const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  if (authorization === undefined) {
    res.statusCode = 400;
    return res.json({ message: "Unauthorized" });
  }

  try {
    const splittedToken = authorization.split(" ")[1];
    console.log(splittedToken);
    const token = await jwt.verify(splittedToken, "sssss");
    req.user = token;
    next();
  } catch (error) {
    res.statusCode = 400;
    return res.json({ message: "Invalid token" });
  }
};

// require("dotenv").config();

// const authMiddleware = async (req, res, next) => {
//   const { authorization } = req.headers;

//   if (authorization === undefined) {
//     res.statusCode = 400;
//     return res.json({ message: "Unauthorized" });
//   }

//   try {
//     const splitedToken = authorization.split(" ")[1];
//     console.log(splitedToken);
//     const token = await jwt.verify(
//       splitedToken,
//       "sssss"
//       // process.env.SECRET_KEYS
//     );
//     req.user = token;
//     next();
//   } catch (error) {
//     res.statusCode = 400;
//     return res.json({ message: "invalid token" });
//   }
// };

module.exports = authMiddleware;
