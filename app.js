const express = require("express");
const cors = require("cors");
const app = express();
const userRouter = require("./users/user_route");
const gameRouter = require("./games/game_route");
const swaggerUi = require("swagger-ui-express");
const gameSwagger = require("./gameSwagger.json");
const port = 8000;

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(gameSwagger));
app.use(cors());
app.use(express.json());
app.use("/users", userRouter);
app.use("/games", gameRouter);

app.get("/", (req, res) => {
  return res.send("Hallo world!");
});

app.get("/ping", (req, res) => {
  return res.send("Welcome");
});

app.listen(port, (req, res) => {
  console.log("This app is running on port " + port);
});
